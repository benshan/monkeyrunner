#!/usr/bin/env monkeyrunner
# coding=utf-8
# Copyright 2010, The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from os.path import os
import sys
import time

from com.android.chimpchat.hierarchyviewer import HierarchyViewer
from com.android.monkeyrunner import MonkeyDevice as md
from com.android.monkeyrunner import MonkeyImage as mi
from com.android.monkeyrunner import MonkeyRunner as mr
from com.android.monkeyrunner.easy import By
from com.android.monkeyrunner.easy import EasyMonkeyDevice


CMD_MAP = {
    'TOUCH': lambda dev, arg: dev.touch(**arg),
    'DRAG': lambda dev, arg: dev.drag(**arg),
    'PRESS': lambda dev, arg: dev.press(**arg),
    'TYPE': lambda dev, arg: dev.type(**arg),
    'WAIT': lambda dev, arg: mr.sleep(**arg)
    }

def process_file(fp, device):
    for line in fp:
        (cmd, rest) = line.split('|')
        try:
            # Parse the pydict
            rest = eval(rest)
        except:
            print 'unable to parse options'
            continue

        if cmd not in CMD_MAP:
            print 'unknown command: ' + cmd
            continue
        CMD_MAP[cmd](device, rest)

#获取脚本文件的当前路径
def cur_file_dir():
    #获取脚本路径
    path = sys.path[0]
    #判断为脚本文件还是py2exe编译后的文件，如果是脚本文件，则返回的是脚本的目录，如果是py2exe编译后的文件，则返回的是编译后的文件路径
    if os.path.isfile(path):
        return os.path.dirname(path)
    return path


def startApp(d, log):
    print("Start startApp")
    pack = "com.bloomlife.luobo"
    act = "com.bloomlife.luobo.activity.SplashActivity"
    compon = pack + '/' +  act
    log.write("启动软件...\n")
    d.startActivity(component = compon)
    mr.sleep(10)
    


def main():
    str = cur_file_dir()
    length = str.find(':')
    path = str[length+1:]
    print("path" + path)
    screenShotPath = path
    #输出日志
    now = time.strftime("%Y-%m-%d-%H-%M-%S")
    logPath = path
    
    names = sys.argv[0].split('\\')
    filename = names[len(names)-1]
    print("++++++++++++++++++++++++++++++++++++++")
    print("Start execute "+filename)
    
    filename.replace('.py', '')
    log = open(logPath+"\\"+filename[:-3]+"-log-"+now+".txt", "w")
    device = mr.waitForConnection()
    log.write("等待手机连接...\n")
    #device.installPackage('d:/monkeyrunner/有道词典V4.0.3.apk'.decode('utf-8'))
    print("The apk has installed successfully")
    startApp(device, log)
    file = sys.argv[1]
    fp = open(file, 'r')
    process_file(fp, device)
    fp.close();
    print("success")
    log.write("执行完毕....\n")
    
if __name__ == '__main__':
    main()