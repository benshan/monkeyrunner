@echo off
setlocal enabledelayedexpansion
echo [Current path]: %cd%\recorder.py
echo [Execute]: Monkeyrunner recorder.py
echo.
echo [Start...]
Monkeyrunner %cd%\recorder.py
echo.
echo [Stop]